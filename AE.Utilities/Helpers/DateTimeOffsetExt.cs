﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlintaEnergy.Utility.Helpers
{
    public static class DateTimeOffsetExt
    {
        public static int GetAge(this DateTimeOffset dtOffset)
        {
            var currentDate = DateTime.UtcNow;
            int age = currentDate.Year - dtOffset.Year;

            if (currentDate < dtOffset.AddYears(age))
            {
                age--;
            }

            return age;
        }
    }
}
