﻿using System;
using System.Collections.Generic;
using System.Text;

using AlintaEnergy.Data.Entities;

namespace AlintaEnergy.Models
{
    public class CustomerCreationModel : CustomerModelValidation
    {
        public Guid Id { get; set; }
        public CustomerCreationModel()
        {

        }
        public CustomerCreationModel(Customer customer)
        {
            this.Id = customer.Id;
            this.FirstName = customer.FirstName;
            this.LastName = customer.LastName;
            this.DateOfBirth = customer.DateOfBirth;

        }
    }
}
