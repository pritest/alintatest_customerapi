﻿using System;
using System.Collections.Generic;
using System.Text;

using AlintaEnergy.Data.Entities;

namespace AlintaEnergy.Models
{
    public class CustomerModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public DateTimeOffset DateOfBirth { get; set; }
        public string Status { get; set; }

        public CustomerModel()
        {

        }

        public CustomerModel(Customer customer)
        {
            this.Id = customer.Id;
            this.FirstName = customer.FirstName;
            this.LastName = customer.LastName;
            this.DateOfBirth = customer.DateOfBirth;
            this.FullName = customer.FirstName + " " + customer.LastName;
            this.Status = customer.Status;
        }
    }
}
