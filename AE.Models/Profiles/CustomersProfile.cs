﻿using AutoMapper;

using AlintaEnergy.Utility.Helpers;
using AlintaEnergy.Data.Entities;

namespace AlintaEnergy.Models.Profiles
{
    public class CustomersProfile : Profile
    {
        public interface IAEDtoMappings
        {
            IMapper GetMapper();
        }

        public class AEDtoMappings : IAEDtoMappings
        {
            private readonly MapperConfiguration mapperConfiguration = new MapperConfiguration(
                cfg =>
                {
                    cfg.CreateMap<Customer, CustomerModel>()
                    .ForMember(
                    dest => dest.FullName,
                    opt => opt.MapFrom(src => $"{src.FirstName} {src.LastName}"));

                });

            public IMapper GetMapper()
            {
                return mapperConfiguration.CreateMapper();
            }
        }
    }
}
