﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlintaEnergy.Models
{
    public class ResponseModel
    {
        public Object Data { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }

        public ResponseModel()
        {

        }

        public ResponseModel(object data, string statusCode, string message)
        {
            this.Data = data;
            this.Message = message;
            this.StatusCode = statusCode;

        }
    }
}
