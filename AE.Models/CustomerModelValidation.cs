﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AlintaEnergy.Models
{
    public abstract class CustomerModelValidation
    {
        [Required(ErrorMessage = "First Name cannot be Empty.")]
        [MaxLength(50, ErrorMessage = "The First Name shouldn't have more than 50 characters.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name cannot be Empty.")]
        [MaxLength(50, ErrorMessage = "The Last Name shouldn't have more than 50 characters.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Date of Birth must be filled out.")]
        public DateTimeOffset DateOfBirth { get; set; }
    }
}
