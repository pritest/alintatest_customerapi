﻿using System;
using System.Threading.Tasks;

using AlintaEnergy.Models;
using AlintaEnergy.Data.Entities;
using AlintaEnergy.Models;

namespace AlintaEnergy.Service.Interfaces
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        Task<ResponseModel> CreateCustomerAsync(CustomerCreationModel customerModel);
        Task<ResponseModel> UpdateCustomerAsync(CustomerUpdateModel customerModel);
        Task<ResponseModel> DeleteCustomerAsync(Guid Id);
        Task<ResponseModel> SearchCustomerAsync(string FirstName, string LastName);

    }
}