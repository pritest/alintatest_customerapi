﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AlintaEnergy.Service.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        public interface IRepository<T> where T : class
        {
            Task CreateAsync(T entity);
            Task CreateBulkAsync(List<T> entities);
            Task UpdateAsync(T entity);
            Task<IReadOnlyCollection<T>> GetAllAsync();
            Task<T> GetAsync(Guid id);
            Task<IReadOnlyCollection<T>> GetAllAsync(Expression<Func<T, bool>> filter);
            Task<bool> RemoveAsync(Guid id);
        }
    }
}
