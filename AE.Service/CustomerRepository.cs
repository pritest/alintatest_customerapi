﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using AlintaEnergy.Models;
using AlintaEnergy.Data;
using AlintaEnergy.Data.Entities;
using AlintaEnergy.Service.Interfaces;
using static AlintaEnergy.Models.Profiles.CustomersProfile;

/// <summary>
/// This CustomerRepository class contains create, edit, delete and search methods.
/// </summary>

namespace AlintaEnergy.Service
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        private readonly AEDbContext _db;
        private readonly IAEDtoMappings _autoMaper;

        public CustomerRepository(AEDbContext db, IAEDtoMappings autoMaper)
            : base(db)
        {
            _db = db;
            _autoMaper = autoMaper;
        }

        public async Task<ResponseModel> CreateCustomerAsync(CustomerCreationModel customerModel)
        {
            var customer = new Customer
            {
                Id = Guid.NewGuid(),
                FirstName = customerModel.FirstName,
                LastName = customerModel.LastName,
                DateOfBirth = customerModel.DateOfBirth,
                Status = "Active"
            };

            await this.CreateAsync(customer);
            var newCustomer = _autoMaper.GetMapper().Map<Customer,CustomerModel>(customer);
            return new ResponseModel(newCustomer, "201", "Customer created successfully");
        }

        public async Task<ResponseModel> DeleteCustomerAsync(Guid Id)
        {
            var customer = await this.GetAsync(Id);
            if (customer != null)
            {
                //Change the status to Inactive of the customer instead of acctually deleting from the db..
                customer.Status = "Inactive";

                await this.UpdateAsync(customer);
                var deletedCustomer = _autoMaper.GetMapper().Map<Customer, CustomerModel>(customer);
                return new ResponseModel(deletedCustomer, "200", "Customer deleted successfully");
            }

            return new ResponseModel(null, "400", "Failed to delete the customer - No customer found on given Id");
        }

        public async Task<ResponseModel> SearchCustomerAsync(string firstName, string lastName)
        {
            if (string.IsNullOrEmpty(firstName) && string.IsNullOrEmpty(lastName))
                return new ResponseModel(null, "404", "FirstName or LastName must be provided");
            IEnumerable<Customer> customers = null;
            if(!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName))
            {
               customers = await this.GetAllAsync(p => p.FirstName.ToLower().Contains(firstName.ToLower()) || p.LastName.ToLower().Contains(lastName.ToLower()));
            }
            else if (!string.IsNullOrEmpty(firstName))
            {
                customers = await this.GetAllAsync(p => p.FirstName.ToLower().Contains(firstName.ToLower()));
            }
            else if (!string.IsNullOrEmpty(lastName))
            {
                customers = await this.GetAllAsync(p => p.LastName.ToLower().Contains(lastName.ToLower()));
            }

            var customerMapper = _autoMaper.GetMapper();
            var customerData = customers.Select(p => customerMapper.Map<Customer, CustomerModel>(p)).ToList();
            //var customerData = customers.Select(p => new CustomerModel(p)).ToList();
            if (customerData.Any())
                return new ResponseModel(customerData, "200", "");
            else
                return new ResponseModel(null, "400", "No records found!");

        }

        public async Task<ResponseModel> UpdateCustomerAsync(CustomerUpdateModel customerModel)
        {

            if (customerModel != null)
            {
                var customer = await this.GetAsync(customerModel.Id);
                if (customer != null)
                {
                    customer.FirstName = customerModel.FirstName;
                    customer.LastName = customerModel.LastName;
                    customer.DateOfBirth = customerModel.DateOfBirth;

                    await this.UpdateAsync(customer);
                    var updatedCustomer = _autoMaper.GetMapper().Map<Customer, CustomerModel>(customer);
                    return new ResponseModel(updatedCustomer, "200", "Customer updated successfully");
                }
            }

            return new ResponseModel(null, "400", "Failed to update the customer..");
        }
    }
}