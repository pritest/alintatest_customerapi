﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

using AlintaEnergy.Data;
using AlintaEnergy.Data.Entities;
using AlintaEnergy.Models;
using AlintaEnergy.Service;
using AlintaEnergy.Service.Interfaces;
using AlintaEnergy.Test.Setup;
using static AlintaEnergy.Models.Profiles.CustomersProfile;

/// <summary>
/// The CustomerTest class contains all the 4 tests methods for the customer
/// including add (Should_Add_New_Customer), edit (Should_Update_Customer),
/// delete (Should_Delete_Customer) and search customer by first name or last name 
/// (Should_Search_Customers_By_FirstName_Or_LastName)
/// </summary>

namespace AlintaEnergy.Test
{
    public class CustomerTest : IClassFixture<CustomerDataSeedFixture>
    {
        private readonly AEDbContext _context;
        private readonly ICustomerRepository _cusRepository;
        public CustomerTest(CustomerDataSeedFixture fixture)
        {
            _context = fixture.Context;
            _cusRepository = new CustomerRepository(_context, new AEDtoMappings());
        }
    

        [Fact]
        public async Task Should_Add_New_Customer()
        {
            var newCustomer = new CustomerCreationModel
            {
                FirstName = "Senu",
                LastName = "Amaya",
                DateOfBirth = new System.DateTimeOffset(new System.DateTime(2013, 10, 29))
            };

            // Act
            var result = await _cusRepository.CreateCustomerAsync(newCustomer);
            var customerData = (CustomerModel)result.Data;
            //Assert
            Assert.True(result.Message == "Customer created successfully", "Failed to create customer");
            Assert.True(customerData.FirstName == "Senu", "Customer First Name is incorrect");
            Assert.True(customerData.LastName == "Amaya", "Customer Last Name is incorrect");// 29/10/2013 12:00:00 AM +11:00
            Assert.True(customerData.DateOfBirth.ToString() == "29/10/2013 12:00:00 AM +11:00", "Date of Birth is not matching");
        }

        [Fact]
        public async Task Should_Update_Customer()
        {
            // Arrange 
            var newId = Guid.NewGuid();
            await _context.Customers.AddAsync(new Customer
            {
                Id = newId,
                FirstName = "Senu",
                LastName = "Amaya",
                DateOfBirth = new System.DateTimeOffset(new System.DateTime(2013, 10, 29))
            });

            await _context.SaveChangesAsync();
            // Act
            var updateCustomer = new CustomerUpdateModel
            {
                Id = newId,
                FirstName = "Senu1",
                LastName = "Amaya1",
                DateOfBirth = new System.DateTimeOffset(new System.DateTime(2013, 10, 30))
            };

            var result = await _cusRepository.UpdateCustomerAsync(updateCustomer);
            var UpdateCustomerData = (CustomerModel)result.Data;

            //Assert
            Assert.True(result.Message == "Customer updated successfully","Failed to update customer");
            Assert.True(UpdateCustomerData.FirstName == "Senu1", "Customer First Name is incorrect");
            Assert.True(UpdateCustomerData.LastName == "Amaya1", "Customer Last Name is incorrect");// 29/10/2013 12:00:00 AM +11:00
            Assert.True(UpdateCustomerData.DateOfBirth.ToString() == "30/10/2013 12:00:00 AM +11:00", "Date of Birth is not matching");
        }

        [Fact]
        public async Task Should_Delete_Customer()
        {
            // Arrange 
            var newId = Guid.NewGuid();
            await _context.Customers.AddAsync(new Customer
            {
                Id = newId,
                FirstName = "Senu",
                LastName = "Amaya",
                DateOfBirth = new System.DateTimeOffset(new System.DateTime(2013, 10, 29))
            });

            await _context.SaveChangesAsync();
            // Act
            var updateCustomer = new CustomerUpdateModel
            {
                Id = newId,
                FirstName = "Senu1",
                LastName = "Amaya1",
                DateOfBirth = new System.DateTimeOffset(new System.DateTime(2013, 10, 30))
            };

            var result = await _cusRepository.DeleteCustomerAsync(newId);
            var UpdateCustomerData = (CustomerModel)result.Data;

            //Assert
            Assert.True(result.Message == "Customer deleted successfully", "Failed to delete customer");
            Assert.True(UpdateCustomerData.Status == "Inactive", "Failed to delete customer");
        }

        [Fact]
        public async Task Should_Search_Customers()
        {
            // Arrange 
            var newId1 = Guid.NewGuid();
            var newId2 = Guid.NewGuid();
            await _context.Customers.AddAsync(new Customer
            {
                Id = newId1,
                FirstName = "Senu1",
                LastName = "Amaya2",
                DateOfBirth = new System.DateTimeOffset(new System.DateTime(2013, 10, 29))
            });

            await _context.SaveChangesAsync();
           
            await _context.Customers.AddAsync(new Customer
            {
                Id = newId2,
                FirstName = "Senu",
                LastName = "Amaya",
                DateOfBirth = new System.DateTimeOffset(new System.DateTime(2013, 10, 29))
            });

            await _context.SaveChangesAsync();

            var result = await _cusRepository.SearchCustomerAsync("Senu", "Amaya");
            var customers = (List<CustomerModel>)result.Data;

            //Assert
            Assert.True(customers.Count == 2, "Failed to search customer");
            Assert.True(customers.All(p=>p.FirstName.Contains("Senu")), "Failed to search customer with first name");
            Assert.True(customers.All(p => p.LastName.Contains("Amaya")), "Failed to search customer with last name");
        }
    }
}
