﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

using AlintaEnergy.Data;

namespace AlintaEnergy.Test.Setup
{
    public class CustomerDataSeedFixture : IDisposable
    {
        public AEDbContext Context => InMemoryContext();
        private AEDbContext InMemoryContext()
        {
            var options = new DbContextOptionsBuilder<AEDbContext>()
                .UseInMemoryDatabase(databaseName: "Test_Database")
                .EnableSensitiveDataLogging()
                .Options;
            var context = new AEDbContext(options);

            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            return context;
        }
        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
