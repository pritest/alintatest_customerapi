﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

using AlintaEnergy.Data.Entities;

namespace AlintaEnergy.Data
{
    public class AEDbContext : DbContext
    {
        public AEDbContext(DbContextOptions<AEDbContext> options)
           : base(options)
        {
           
        }

        public DbSet<Customer> Customers { get; set; }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // seed the database with dummy data
            modelBuilder.Entity<Customer>().HasData(
                new Customer()
                {
                    Id = Guid.Parse("d28888e9-2ba9-473a-a40f-e38cb54f9b35"),
                    FirstName = "Deen",
                    LastName = "Barker",
                    DateOfBirth = new DateTime(1950, 8, 20)

                },
                new Customer()
                {
                    Id = Guid.Parse("da2fd609-d754-4feb-8acd-c4f9ff13ba96"),
                    FirstName = "Alex",
                    LastName = "Williams",
                    DateOfBirth = new DateTime(1998, 1, 21)
                },
                new Customer()
                {
                    Id = Guid.Parse("2902b665-1190-4c70-9915-b9c2d7680450"),
                    FirstName = "John",
                    LastName = "Fitzgerald",
                    DateOfBirth = new DateTime(1991, 10, 11)
                }
                );
      

            base.OnModelCreating(modelBuilder);
        }
    }
}
