﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlintaEnergy.Models;
using AlintaEnergy.Service.Interfaces;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AlintaEnergy.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private static ICustomerRepository _customerRepository;
        public CustomerController(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> CreateCustomer([FromBody] CustomerCreationModel customerCreationModel)
        {
            var result = await _customerRepository.CreateCustomerAsync(customerCreationModel);
            return Ok(result);
        }

        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> UpdateCustomer([FromBody] CustomerUpdateModel customerUpdateModel)
        {
            var result = await _customerRepository.UpdateCustomerAsync(customerUpdateModel);
            return Ok(result);
        }

        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> DeleteCustomer(Guid Id)
        {
            var result = await _customerRepository.DeleteCustomerAsync(Id);
            return Ok(result);
        }

        [HttpPost]
        [Route("Search")]
        public async Task<IActionResult> SearchCustomer(string fisrtName, string lastName)
        {
            var result = await _customerRepository.SearchCustomerAsync(fisrtName, lastName);
            return Ok(result);
        }
    }
}
